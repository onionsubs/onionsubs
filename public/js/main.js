console.log(
  `                                                     
                                      ==-:.                                               
                                     *@@@@@@@%##*-                                        
                                    *@@@%#%@@@@@@@+                                       
                                  .%@@@#     .:#@@@%:                                     
                                 =@@@@+      :: +@@@@#-                                   
                              .=@@@@%:  :-   +@* .*@@@@@*=.                               
                           :+#@@@@%=   :@%    -@%:  =#@@@@@@*=.                           
                        .+%@@@@@*-    =@#      .*@#=.  -+#@@@@@#=                         
                      :#@@@@@+:       :-          -+:      -*@@@@@+.                      
                    .*@@@@#-                                  -%@@@@+                     
                   =@@@@*.                                      :%@@@%-                   
                  #@@@%:                                          -@@@@*                  
                 #@@@*                                             .#@@@#                 
                *@@@*        :                                       #@@@*                
               =@@@#       .#=                    .                   %@@@-               
               @@@@:      +@#                      #.                 :@@@%               
              :@@@%     .%@@         .             +%           -.     %@@@:              
              =@@@*    .@@@=        :=             -@#          :%     +@@@=              
              =@@@*   .@@@@         @:             :@@+          @+    =@@@+              
              -@@@%   #@@@+        +@              :@@@.         @@    +@@@=              
               @@@@: :@@@@.        @%              =@@@*         %@.   @@@@.              
               =@@@% *@@@@        -@*              *@@@@.        %@-  +@@@*               
                *@@@#%@@@#        +@+              @@@@@-        @@- -@@@%                
                 *@@@@@@@%        #@+             -@@@@@-       :@@:=@@@%                 
                  =@@@@@@@        %@+             #@@@@@-       *@@@@@@#                  
                   .#@@@@@+       %@*            :@@@@@@       -@@@@@@=                   
                     :#@@@@@*-.   %@@            *@@@@@=     -*@@@@@*.                    
                       .=%@@@@@@*=@@@+          :@@@@@*.:=*%@@@@@%=.                      
                          .-*@@@@@@@@@%#*+++++=+%@@@@@@@@@@@@@#=.                         
                              .:=*#@@@@@@@@@@@@@@@@@@@@@@#+=:                             
                                     .:-==++++++++=--:.         
  `
)


function rep(w1, w2) {
  document.body.innerHTML = document.body.innerHTML.replace(w1, w2);
}

const customex = function(){
  let nome = localStorage.getItem("nome");
  let pronome = localStorage.getItem("pronome");

if (typeof nome !== "object") {
  rep("usuário", nome.charAt(0).toUpperCase() + nome.slice(1));
  if(document.querySelector("#user")){
    document.querySelector("#user").querySelector("span").textContent = nome.charAt(0).toUpperCase() + nome.slice(1);
  }
 

  if (document.querySelector("#userpfp")) {
    document.querySelector("#userpfp").href.baseVal =
      localStorage.getItem("pfp");
  }
}
/*
if (typeof pronome !== "object") {
  if (pronome === "elu/delu") {
  } else {
    if (pronome === "Ela/Dela") {
      rep("Bem-vinde", "Bem-vinda");
    }
    if (pronome === "Ele/Dele") {
      rep("Bem-vinde", "Bem-vindo");
    }
  }
}
*/

if (typeof pronome !== "object") {
  pronome === "Ela/Dela" ? rep("Bem-vinde", "Bem-vinda") : pronome === "Ele/Dele" ? rep("Bem-vinde", "Bem-vindo") : {}
}
}
customex()