
var Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.onmouseenter = Swal.stopTimer;
    toast.onmouseleave = Swal.resumeTimer;
  }
});

function checkeps() {
  const eps = document.getElementsByClassName("episodio");
  if (JSON.parse(localStorage.getItem("watched"))) {
    const array = JSON.parse(localStorage.getItem("watched"));
    //console.log(array)
    Object.values(eps).forEach((ep) => {
      if (ep.href) {
        let url = new URL(ep.href);
        let href = decodeURI(url.pathname + url.search);
        if (array.includes(href)) {
          ep.classList.add("watched");
        }
      } else {
        if (ep.dataset.url) {
          if (array.includes(ep.dataset.url)) {
            ep.classList.add("watched");
          }
        }
      }
    });
  }
}

function watch(nome) {
  const arr = JSON.parse(localStorage.getItem("watched")) || [];
  if (arr.indexOf(nome) <= -1) {
    console.log(nome);
    //console.log(arr.indexOf(nome))
    arr.push(nome);
    localStorage.setItem("watched", JSON.stringify(arr));
  }
}
  //var cookie = document.cookie;
  //var output = {};
  //cookie.split(/\s*;\s*/).forEach(function (pair) {
  //pair = pair.split(/\s*=\s*/);
  //output[pair[0]] = pair.splice(1).join("=");
  //});/*
  //var json = JSON.stringify(output, null, 4);
  //if (json.includes("<%= nomeplay %>")) {
  //document.getElementById("<%= nomeplay %>btn").classList.add("bg-amber-500")
  //}


function fav(event) {
  console.log(event);
  if (!event) return;
  var btn = event;
  if (btn.tagName == "I") btn = btn.parentElement;
  console.log(btn.tagName);

  let nome = btn.dataset.sin;
  let id = nome
    .split(" ")
    .slice(0, 2)
    .join("")
    .replace(/[^a-zA-Z0-9 ]/g, "")
    .toLowerCase();

  if (btn.classList.contains("bg-amber-500")) {
    let favst = JSON.parse(localStorage.getItem("fav")) || [];
    Toast.fire({
      icon: "error",
      title: "removendo " + nome + " dos favoritos! ❌⭐",
    });
    btn.classList.remove("bg-amber-500");

    console.log(favst);
    const nfavs = favst.filter(function (awa) {
      return awa !== id;
    });
    console.log(favst);

    localStorage.setItem("fav", JSON.stringify(nfavs));

    if (window.location.href == "https://site.onionsubs.repl.co/user") {
      document.getElementById(nome + "art").remove();
    }
    return;
  } else {
    let favst = JSON.parse(localStorage.getItem("fav")) || [];
    Toast.fire({
      icon: "success",
      title: "adicionando " + nome + " aos favoritos! ✔️⭐",
    });

    console.log(favst);
    favst.push(id);
    console.log(favst);

    localStorage.setItem("fav", JSON.stringify(favst));

    btn.classList.add("bg-amber-500");
  }
}
  //var btns = document.getElementsByClassName('btn-particles');
 function boom(e, btn) {
      if (btn.parents().children(".btn-particles").length > 0) return;
      var colors = [
        //"#ffb3f6",
        "rgb(251 191 36)",
        "#f0cc71",
        //"#7aa0ff",
        //"#333",
        // '#FFD100',
        // '#FF9300',
        // '#FF7FA4'
      ];
      var shapes = [
        '<polygon class="star" points="21,0,28.053423027509677,11.29179606750063,40.97218684219823,14.510643118126104,32.412678195541844,24.70820393249937,33.34349029814194,37.989356881873896,21,33,8.656509701858067,37.989356881873896,9.587321804458158,24.70820393249937,1.0278131578017735,14.510643118126108,13.94657697249032,11.291796067500632"></polygon>',
        // '<path class="circle" d="m 20 1 a 1 1 0 0 0 0 25 a 1 1 0 0 0 0 -25"></path>',
        '<polygon class="other-star" points="18,0,22.242640687119284,13.757359312880714,36,18,22.242640687119284,22.242640687119284,18.000000000000004,36,13.757359312880716,22.242640687119284,0,18.000000000000004,13.757359312880714,13.757359312880716"></polygon>',
        '<polygon class="diamond" points="18,0,27.192388155425117,8.80761184457488,36,18,27.19238815542512,27.192388155425117,18.000000000000004,36,8.807611844574883,27.19238815542512,0,18.000000000000004,8.80761184457488,8.807611844574884"></polygon>',
      ];

      btn.parent().append('<div class="btn-particles"></div>');
      var group = [];
      var num = Math.floor(Math.random() * 25) + 25;

      for (i = 0; i < num; i++) {
        var randBG = Math.floor(Math.random() * colors.length);
        var getShape = Math.floor(Math.random() * shapes.length);
        var c = Math.floor(Math.random() * 10) + 5;
        var scale = Math.floor(Math.random() * (8 - 4 + 1)) + 4;
        var x = Math.floor(Math.random() * (150 + 100)) - 100;
        var y = Math.floor(Math.random() * (150 + 100)) - 175;
        var sec = Math.floor(Math.random() * 1700) + 1500;
        var cir = $('<div class="cir"></div>');
        var shape = $('<svg class="shape">' + shapes[getShape] + "</svg>");

        shape.css({
          top: e.pageY - btn.offset().top - 50,
          left: e.pageX - btn.offset().left,
          transform: "scale(0." + scale + ")",
          transition: sec + "ms",
          fill: colors[randBG],
        });

        btn.parents().children(".btn-particles").append(shape);

        group.push({ shape: shape, x: x, y: y });
      }

      for (var a = 0; a < group.length; a++) {
        var shape = group[a].shape;
        var x = group[a].x,
          y = group[a].y;
        shape.css({
          left: x,
          top: y,
          transform: "scale(0)",
        });
      }

      setTimeout(function () {
        /*for (var b = 0; b < group.length; b++) {
          var shape = group[b].shape;
          shape.remove();
        }*/
        btn.parents().children(".btn-particles").remove();
        group = [];
      }, sec);
    };