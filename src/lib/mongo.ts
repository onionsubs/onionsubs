import { MongoClient } from "mongodb";
const client = new MongoClient(process.env.mongodburi);

export var db = client.db("onion");

if (!process.env.mongodburi) {
  throw new Error('Invalid environment variable: "MONGODB_URI"');
}

if (process.env.NODE_ENV === "development") {
    if (!global._mongoConnection) {

      global._mongoConnection = db

      db = global._mongoConnection;
    }}
  

export const Datadb = db.collection("data")

export const latest = await db.collection("data").find().limit(1).sort({ _id: -1 }).toArray();