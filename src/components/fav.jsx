import { useState, useEffect } from 'react';

function Fav(props) {
  const [favorites, setFavorites] = useState([]);
  const [favoritesFromDB, setFavoritesFromDB] = useState([]); // Novo estado para os favoritos do banco de dados

useEffect(() => {
  const fetchData = async () => {
    const favData = JSON.parse(localStorage.getItem("fav")) || [];
    setFavorites(favData);

    if (favData.length > 0) {
      const dbFavorites = props.db.filter((item) => favData.includes(item.id));
      setFavoritesFromDB(dbFavorites);
    }
  };

  fetchData();
}, [props.db]);
; // Adiciona props.db como dependência do useEffect

  if (typeof window === 'undefined' && favorites.length === 0) {
    return (
      <div className="flex flex-col items-center justify-center">
        <p>
          Você ainda não favoritou nada! Para favoritar, basta clicar neste
          botão quando gosta de algo!
        </p>
        <div>
          <img
            className="rounded-lg"
            src="https://media.discordapp.net/attachments/315474658453356544/1070914592273858570/image.png"
          />
        </div>
      </div>
    );
  }

  return (
    <div className="flex justify-center gap-5 flex-warp">
      {favoritesFromDB.map((serie, index) => (
        <article
          key={index}
          id={serie.title + "art"}
          className="min-w-full sm:min-w-0 w-full lg:w-1/3 2xl:w-1/4 bg-[var(--bg-2)] rounded-lg shadow-xl"
        >
          <div
            className={`w-full h-40 block bg-[var(--second-text-color)] rounded-t-lg
       bg-cover`}
            style={{
              backgroundImage: `url(${serie.img})`,
              backgroundPosition: "center",
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
            }}
          >
            <div
              className={
                "border-b-2 block rounded-t-lg w-full h-full" +
                ` border-[${serie.rgb}]`
              }
            ></div>
          </div>
          <div className="flex flex-col p-5" title={serie.title}>
            <h1 className="font-xl font-bold">{serie.title}</h1>
            <span
              title={serie.desc}
              className="text-clip italic text-sm mt-3 h-10 overflow-hidden"
            >
              {serie.desc}
            </span>
            <div className="flex flex-col md:flex-row space-y-3 md:space-y-0 md:space-x-3 xl:space-x-5 mt-5 text-white">
              <a href={`/${serie.id}`} className="w-full">
                <button
                  onMouseOver={(event) => {
                    event.stopPropagation();
                    event.target.style.background = serie.rgb;
                  }}
                  onMouseOut={(event) => {
                    event.stopPropagation();
                    event.target.style.background = "var(--prime-color)";
                  }}
                  className="w-full bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300"
                  data-ripple-light="true"
                >
                  <i
                    onMouseOver={(event) => {
                      event.stopPropagation();
                      event.target.parentElement.style.background = serie.rgb;
                    }}
                    onMouseOut={(event) => {
                      event.stopPropagation();
                      event.target.parentElement.style.background =
                        "var(--prime-color)";
                    }}
                    className="fa-solid fa-play"
                  ></i>
                </button>
              </a>

              <div className="w-full md:w-3/6">
                <button
                  data-sin={serie.title}
                  onClick={(event) => {
                    event.stopPropagation();
                    fav(event.target);
                  }}
                  id={`${serie.id}btn`}
                  className={
                    "w-full favbtn bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300 hover:bg-amber-400" +
                    (favorites.includes(serie.id) ? " bg-amber-500" : "")
                  }
                  data-ripple-light="true"
                >
                  <i className="fa-duotone fa-stars" />
                </button>
              </div>
            </div>
          </div>
        </article>
      ))}
    </div>
  );
}

export default Fav;