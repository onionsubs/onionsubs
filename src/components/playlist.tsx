import React, { useState, useEffect, type FormEvent } from "react";

interface Props {
  title: string;
  body: string;
  id: string;
  image: string;
  favv?: string;
  dash?: string;
  link?: string;
  rgb?: string;
  neww?: string;
  db?: any;
  bintent?: string;
  editbuttonhref?: any;
  links?: any;
  delButton?: {
    intent: string;
    showID?: string;
    seasonId?: string;
    epId?: any;
  };
}

const playlist = ({
  title,
  body,
  id,
  image,
  favv,
  bintent,
  dash,
  link,
  rgb,
  links,
  neww,
  db,
  editbuttonhref,
  delButton,
}: Props) => {
  const [responseMessage, setResponseMessage] = useState("");

  const ref = React.useRef(null);

  var urlAtual: string;
  useEffect(() => {
    // Obter a URL atual
    urlAtual = window.location.href;

    // Verificar se a URL atual já tem parâmetros
    if (urlAtual.includes("?")) {
      // Se já existem parâmetros, adicionar o novo com um caractere de ampersand
      urlAtual += "&func=create";
    } else {
      // Se não existem parâmetros, adicionar o novo com um ponto de interrogação
      urlAtual += "?func=create";
    }
  }, []);

  async function submit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const formData = new FormData(e.target as HTMLFormElement);
    const response = await fetch("/api/serie", {
      method: "POST",
      body: formData,
    });
    const data = await response.json();
    if (data.message) {
      setResponseMessage(data.message);
    }
  }
  const remove = (el) => {
    var element = el;
    element.remove();
  };
  const [hovered, setHovered] = useState(false);
  return (
    <article
      ref={ref}
      id={title + "art"}
      className="min-w-full sm:min-w-0 w-full lg:w-1/3 2xl:w-1/4 bg-[var(--bg-2)] rounded-lg shadow-xl"
      style={{ transitionDuration: "500ms" }}
    >
      <div className="w-full h-40 block bg-[var(--second-text-color)] rounded-t-lg">
        <div
          className="border-b-2 block rounded-t-lg w-full h-full"
          style={{
            background: `url(${image}) no-repeat center center`,
            WebkitBackgroundSize: "cover",
            MozBackgroundSize: "cover",
            OBackgroundSize: "cover",
            backgroundSize: "cover",
            borderColor: rgb ? rgb : "grey",
          }}
        />
      </div>
      <div className="flex flex-col p-5" title={title}>
        <h1 className="font-xl font-bold">{title}</h1>
        <span
          className="text-clip italic text-sm mt-3 h-10"
          style={{
            overflow: "hidden",
            textOverflow: "ellipsis",
            display: "-webkit-box",
            WebkitLineClamp: 2,
            lineClamp: 2,
            WebkitBoxOrient: "vertical",
          }}
        >
          {body}
        </span>
        <div className="flex flex-col md:flex-row space-y-3 md:space-y-0 md:space-x-3 xl:space-x-5 mt-5 text-white">
          {dash !== "true" && neww !== "true" ? (
            <a href={`/${id}`} className="w-full">
              <button
                onMouseOver={() => setHovered(true)}
                onMouseOut={() => setHovered(false)}
                className="w-full bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300"
                style={{
                  backgroundColor: hovered ? rgb : "var(--prime-color)",
                }}
                data-ripple-light="true"
              >
                <i className="fa-solid fa-play" />
              </button>
            </a>
          ) : null}
          {favv === "true" ? (
            <button
              data-sin={title}
              onClick={(event) => {
                fav(event.target);
              }}
              id={`${id}btn`}
              className="w-full favbtn bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300 hover:bg-amber-400"
              data-ripple-light="true"
            >
              <i className="fa-duotone fa-stars" />
            </button>
          ) : null}
          {dash === "true" ? (
            <>
              <div className="w-full">
                {bintent === "episodeDashboard" ? (
                  <button
                    className="w-full bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300 hover:bg-blue-400"
                    data-ripple-light="true"
                    onClick={async (e) => {
                      //@ts-ignore
                     const { value: formValues } = await Swal.fire({
                       customClass: {
                         confirmButton:
                           "m-2 text-lg text-white w-full bg-[var(--prime-color)] rounded-md text-center p-2 px-3 ease-out duration-300 hover:bg-green-400",
                       },
                       title: `Editar ${title}`,
                       showCloseButton: true,
                       confirmButtonText: `<i class="fa-duotone fa-square-check"></i> salvar`,
                       buttonsStyling: false,
                       html: `

                        <form id="formedit">
                    <input type="hidden" name="intent" id="intent" value="edit"/>
                    
                    <input type="hidden" id="seasonId" name="seasonId" class="swal2-input" placaholder="seasonId" value="${delButton?.seasonId}" readOnly/>

                    <input type="hidden" id="showId" name="showId" class="swal2-input" placaholder="showId" value="${delButton?.showID}" readOnly/>

                    <input type="hidden" id="epId" name="epId" class="swal2-input" placaholder="epId" value="${title}" readOnly/>


                        <input id="ep" name="ep" class="swal2-input" placaholder="title" value="${title}"/>
                        <input id="sinopse" name="sinopse" class="swal2-input" placaholder="sinopse" value="${body}"/>
                        <textarea id="link" name="link" class="swal2-input pt-2" placaholder="links (separados por ,)" value="">${links.toString()} </textarea>
                        <input id="img" name="img" class="swal2-input" placaholder="img" value="${image}"/>

                        <form/>
                        `,
                     }).then(async (result) => {
                       if (result.isConfirmed) {
                         const form = document.getElementById(
                           "formedit"
                         ) as HTMLFormElement; //(getElementbyId as HTMLFormElement;
                         const formData = new FormData(form as HTMLFormElement);
                         const response = await fetch("/api/serie", {
                           method: "POST",
                           body: formData,
                         });
                         const data = await response.json();
                         if (data.message) {
                           setResponseMessage(data.message);
                         }

                         /*
                         setTimeout(function () {
                           remove(ref.current);
                         }, 1000);
                         */
                       } else {
                         return;
                       }
                     });
                  }}
                  >
                    <i className="fa-duotone fa-pencil" />
                  </button>
                ) : (
                  <a href={editbuttonhref ? editbuttonhref : `?edit=${id}`}>
                    <button
                      className="w-full bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300 hover:bg-blue-400"
                      data-ripple-light="true"
                    >
                      <i className="fa-duotone fa-pencil" />
                    </button>
                  </a>
                )}
              </div>
              <div className="w-full">
                <form method="POST" onSubmit={submit} ref={ref}>
                  {delButton?.showID ? (
                    <input
                      type="hidden"
                      id="showId"
                      name="showId"
                      value={delButton?.showID}
                      readOnly
                    />
                  ) : null}
                  {delButton?.seasonId ? (
                    <input
                      type="hidden"
                      id="seasonId"
                      name="seasonId"
                      value={delButton?.seasonId}
                      readOnly
                    />
                  ) : null}
                  {delButton?.epId ? (
                    <input
                      type="hidden"
                      id="epId"
                      name="epId"
                      value={delButton?.epId}
                      readOnly
                    />
                  ) : null}
                  <input
                    type="hidden"
                    name="intent"
                    id="intent"
                    value={delButton?.intent || "delet"}
                  />
                  <input type="hidden" name="id" id="id" value={id} />
                </form>

                <button
                  className="w-full bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300 hover:bg-rose-400"
                  data-ripple-light="true"
                  onClick={(e) => {
                    //@ts-ignore
                    Swal.fire({
                      customClass: {
                        confirmButton:
                          "m-2 text-lg text-white w-full bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300 hover:bg-rose-400",
                        cancelButton:
                          "m-2 text-white w-full bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300 hover:bg-rose-400",
                      },
                      title: `certeza?`,
                      showCloseButton: true,
                      showCancelButton: true,
                      confirmButtonText: `<i class="fa-duotone fa-square-check"></i> sim`,
                      cancelButtonText: `<i class="fa-duotone fa-xmark-large"></i> nn :(`,
                      buttonsStyling: false,
                    }).then(async (result) => {
                      if (result.isConfirmed) {
                        const form = (
                          (e.target as HTMLButtonElement)
                            .parentElement as HTMLDivElement
                        ).firstChild as HTMLFormElement;
                        const formData = new FormData(form as HTMLFormElement);
                        const response = await fetch("/api/serie", {
                          method: "POST",
                          body: formData,
                        });
                        const data = await response.json();
                        if (data.message) {
                          setResponseMessage(data.message);
                        }
                        setTimeout(function () {
                          remove(ref.current);
                        }, 1000);
                      } else {
                        return;
                      }
                    });
                  }}
                >
                  <i className="fa-duotone fa-trash" />
                </button>
                {responseMessage && <p>{responseMessage}</p>}
              </div>
            </>
          ) : null}

          {/*{title === "Nova temporada" ? "&func=create" : "?func=create"}*/}
          {neww === "true" ? (
            <div className="w-full">
              <button
                onClick={() => window.location.assign(urlAtual)}
                className="w-full bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300 hover:bg-purple-400"
                data-ripple-light="true"
              >
                <i className="fa-duotone fa-plus" />
              </button>
            </div>
          ) : null}
          {typeof link !== "undefined" ? (
            <a href={link} className="w-full md:w-3/6">
              <button
                className="w-full bg-[var(--prime-color)] rounded-md text-center py-2 ease-out duration-300 hover:bg-blue-400"
                data-ripple-light="true"
              >
                <i className="fa-duotone fa-link" />
              </button>
            </a>
          ) : null}
        </div>
      </div>
    </article>
  );
};

export default playlist;
