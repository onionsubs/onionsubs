import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Pagination, Navigation } from "swiper/modules";

export default function MySwiper(props: any) {
  return (
    <>
      <Swiper
        slidesPerView={1}
        spaceBetween={30}
        loop={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Pagination, Navigation]}
        className="mySwiper"
      >
        <SwiperSlide className="z-10 flex justify-around items-center">
          <div>
            <h1 className="text-4xl font-bold tracking-tight text-[var(--first-text-color)]">
              Bem-vinde à {` `}
              <span className="magic">
                <span className="magic-star">
                  <svg viewBox="0 0 512 512">
                    <path d="M512 255.1c0 11.34-7.406 20.86-18.44 23.64l-171.3 42.78l-42.78 171.1C276.7 504.6 267.2 512 255.9 512s-20.84-7.406-23.62-18.44l-42.66-171.2L18.47 279.6C7.406 276.8 0 267.3 0 255.1c0-11.34 7.406-20.83 18.44-23.61l171.2-42.78l42.78-171.1C235.2 7.406 244.7 0 256 0s20.84 7.406 23.62 18.44l42.78 171.2l171.2 42.78C504.6 235.2 512 244.6 512 255.1z"></path>
                  </svg>
                </span>
                <span className="magic-star">
                  <svg viewBox="0 0 512 512">
                    <path d="M512 255.1c0 11.34-7.406 20.86-18.44 23.64l-171.3 42.78l-42.78 171.1C276.7 504.6 267.2 512 255.9 512s-20.84-7.406-23.62-18.44l-42.66-171.2L18.47 279.6C7.406 276.8 0 267.3 0 255.1c0-11.34 7.406-20.83 18.44-23.61l171.2-42.78l42.78-171.1C235.2 7.406 244.7 0 256 0s20.84 7.406 23.62 18.44l42.78 171.2l171.2 42.78C504.6 235.2 512 244.6 512 255.1z"></path>
                  </svg>
                </span>
                <span className="magic-star">
                  <svg viewBox="0 0 512 512">
                    <path d="M512 255.1c0 11.34-7.406 20.86-18.44 23.64l-171.3 42.78l-42.78 171.1C276.7 504.6 267.2 512 255.9 512s-20.84-7.406-23.62-18.44l-42.66-171.2L18.47 279.6C7.406 276.8 0 267.3 0 255.1c0-11.34 7.406-20.83 18.44-23.61l171.2-42.78l42.78-171.1C235.2 7.406 244.7 0 256 0s20.84 7.406 23.62 18.44l42.78 171.2l171.2 42.78C504.6 235.2 512 244.6 512 255.1z"></path>
                  </svg>
                </span>
                <span className="magic-text">Onion Subs</span>
              </span>
              ! 🧅
            </h1>
            <p>Legendamos desenhos!</p>
          </div>
          <div className="max-[600px]:hidden">
            <img className="w-60" src="/assets/onionimg.svg" />
          </div>
        </SwiperSlide>

        <SwiperSlide className="p-4">
          <div
            className={`overflow-hidden flex justify-around items-center rounded-lg min-h-32 w-full h-full bg-center bg-cover bg-[url('https://image.tmdb.org/t/p/original/${props.latest.backdrop_path}')]`}
          >
            <div className="bg-gradient-to-r from-[var(--bg-1)]">
              <div className="w-1/2  p-6 flex justify-around items-center flex-col gap-2">
                <img
                  className="w-1/2"
                  src={
                    "https://image.tmdb.org/t/p/original/" +
                    props.imgs.logos[0].file_path
                  }
                ></img>
                <p className="w-5/6">{props.latest.overview}</p>
              </div>
            </div>
          </div>
        </SwiperSlide>
      </Swiper>
    </>
  );
}
