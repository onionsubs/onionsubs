import { type FormEvent, useState } from "react";
interface Props {
  title?: string;
  infields?: string[];
  innonreq?: string[];
  intent?: string;
  showId?: string;
  disc?: string;
  seasonId?: string;
}
const modalForm = ({
  intent,
  disc,
  infields,
  innonreq,
  title,
  showId,
  seasonId,
}: Props) => {
  const [responseMessage, setResponseMessage] = useState("");

  async function submit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();
    const formData = new FormData(e.target as HTMLFormElement);
    const response = await fetch("/api/serie", {
      method: "POST",
      body: formData,
    });
    const data = await response.json();
    if (data.message) {
      setResponseMessage(data.message);
    }
  }

  let fields = infields
    ? infields
    : ["title", "desc", "rgb", "img", "originallink"];
  let nonreq = innonreq ? innonreq : ["originallink"];
  return (
    <div>
      <div className="fixed w-full h-full bg-black left-0 top-0 opacity-50 z-10"></div>
      <div className="overflow-hidden fixed z-20 h-3/4 w-1/2 m-auto inset-x-0 inset-y-0 bg-[var(--bg-1)] rounded-lg">
        <div className="sticky w-full top-0 flex items-center justify-between bg-[var(--bg-15)] p-2">
          <p className="font-xl font-bold text-[var(--first-text-color)]">
            {title || "Nova série"}
          </p>
          <button
            onClick={() => {
              const urlAtual = new URL(window.location.href);
              urlAtual.searchParams.delete("func");

              // Atualizar a URL
              window.location.assign(urlAtual);
              console.log(urlAtual)
            }}
            className="bg-[var(--prime-color)] rounded-lg relative px-2 py-1 ease-out duration-300 hover:bg-rose-400"
          >
            <i className="fa-duotone fa-xmark-large"></i>
          </button>
        </div>

        <div className="p-4 h-full">
          <p className="text-center text-slate-400">{disc}</p>
          <form
            className="h-2/4 flex gap-2 flex-col items-center justify-center"
            onSubmit={submit}
          >
            {intent
              ? showId && (
                  <input
                    type="hidden"
                    id="showId"
                    name="showId"
                    value={showId}
                    readOnly
                  />
                )
              : null}
            {intent
              ? seasonId && (
                  <input
                    type="hidden"
                    id="seasonId"
                    name="seasonId"
                    value={seasonId}
                    readOnly
                  />
                )
              : null}
            <input
              type="hidden"
              id="intent"
              name="intent"
              value={intent || "new"}
              readOnly
            />
            {fields.map((field, index) => (
              <div key={index}>
                {field === "link" ? (
                  <textarea
                    className="bg-[var(--bg-15)] border-0 rounded-md p-2 focus:bg-[var(--bg-1)] focus:outline-none focus:ring-1 focus:ring-[var(--prime-color)] transition ease-in-out duration-150"
                    placeholder={field}
                    id={field}
                    name={field}
                    autoComplete={field}
                    required={nonreq.includes(field) ? false : true}
                  ></textarea>
                ) : (
                  <input
                    placeholder={field}
                    className="bg-[var(--bg-15)] border-0 rounded-md p-2 focus:bg-[var(--bg-1)] focus:outline-none focus:ring-1 focus:ring-[var(--prime-color)] transition ease-in-out duration-150"
                    type="text"
                    id={field}
                    name={field}
                    autoComplete={field}
                    required={nonreq.includes(field) ? false : true}
                  />
                )}
              </div>
            ))}

            <button
              className=" bg-[var(--prime-color)] rounded-md text-center p-2 ease-out duration-300 text-white hover:bg-blue-400"
              data-ripple-light="true"
            >
              <i className="fa-duotone fa-paper-plane"></i> Send
            </button>
            {responseMessage && <p>{responseMessage}</p>}
          </form>
        </div>
      </div>
    </div>
  );
};
export default modalForm;
