import type { APIRoute } from "astro";
//import {MongoClient} from "mongodb"
//@ts-ignore
//const client = new MongoClient(process.env.mongodburi);
//const db = client.db('onion');

import { db } from "../../lib/mongo.ts";

export const prerender = false

export const POST: APIRoute = async ({ request }) => {
  //export async function post({ request }) {
  const data = await request.formData();
  const intent = data.get("intent");

  const authorizationHeader = request.headers.get('Authorization');

  // Check if the user is logged in
  if (!authorizationHeader) {
    return new Response(
      JSON.stringify({
        message: "Unauthorized - Missing authorization token",
      }),
      { status: 401 }
    );
  }

  if (intent === "new") {
    let id = data.get("title").toString().split(" ")
      .slice(0, 2)
      .join("")
      .replace(/[^a-zA-Z0-9 ]/g, "")
      .toLowerCase()
    await db.collection("data").insertOne({
      id: id,
      title: data.get("title"),
      rgb: data.get("rgb"),
      img: data.get("img"),
      desc: data.get("desc"),
      originallink: data.get("originallink")! || false,
      seasons: [],

    }).then(awa =>
      console.log(awa)
    )

    return new Response(
      JSON.stringify({
        message: "added!"
      }),
      { status: 200 }
    );

  } else if (intent === "delet") {

    await db.collection("data").findOneAndDelete({ id: data.get('id') })

    return new Response(
      JSON.stringify({
        message: ("recieved request: " + intent)
      }),
      { status: 200 }
    );

  } else if (intent === "edit") {

    if (data.get('epId')) {
      console.log(data)
      //console.log((data.get("link") as string)?.split(",").map(link => link.trim()))
      //@ts-ignore
      //console.log(data.get("link")?.split(",").map(link => link.trim()))
      const showId = data.get("showId");
      const seasonTitle = data.get("seasonId");
      const epId = data.get("epId");

      const newEpisode = {
        ep: data.get("ep"),
        link: (data.get("link") as string).split(",").map(link => link.trim()), // Separar os links por vírgula,
        sinopse: data.get("sinopse"),
        img: data.get("img"),
      };

      await db.collection("data").findOneAndUpdate(
        { id: showId, "seasons.n": seasonTitle, "seasons.eps.ep": epId },
        {
          $set: {
            "seasons.$[s].eps.$[e]": newEpisode
          }
        },
        {
          arrayFilters: [
            { "s.n": seasonTitle },
            { "e.ep": epId }
          ]
        }
      );



      return new Response(
        JSON.stringify({
          message: "Received request: " + intent,
        }),
        { status: 200 }
      );

    }

    
    return new Response(
      JSON.stringify({
        message: ("recieved request: " + intent)
      }),
      { status: 200 }
    );

  } else if (intent === "delet-season") {

    await db.collection("data").findOneAndUpdate({ id: data.get("showId") },
      //@ts-ignore
      { $pull: { 'seasons': { n: data.get("id") } } }
    )

    return new Response(
      JSON.stringify({
        message: ("recieved request: " + intent)
      }),
      { status: 200 }
    );

  } else if (intent === "new-season") {

    await db.collection("data").findOneAndUpdate({ id: data.get("showId") },
      {
        //@ts-ignore
        $push: { seasons: {
            n: data.get("title"),
            eps: []
          }
        }
      })

    return new Response(
      JSON.stringify({
        message: ("recieved request: " + intent)
      }),
      { status: 200 }
    );

  } else if (intent === "new-ep") {
    //@ts-ignore
    console.log(data.get("link").split(",").map(link => link.trim()))
    const showId = data.get("showId");
    const seasonTitle = data.get("seasonId");
    const newEpisode = {
      ep: data.get("ep"),
      //@ts-ignore
      link: data.get("link").split(",").map(link => link.trim()), // Separar os links por vírgula,
      sinopse: data.get("sinopse"),
      img: data.get("img"),
    };

    await db.collection("data").findOneAndUpdate(
      { id: showId, "seasons.n": seasonTitle },
      {
        //@ts-ignore
        $push: { "seasons.$.eps": newEpisode },
      }
    );

    return new Response(
      JSON.stringify({
        message: "Received request: " + intent,
      }),
      { status: 200 }
    );
  } else if (intent === "delet-ep") {
    const showId = data.get("showId");
    const seasonTitle = data.get("seasonId");
    const eptitle = data.get("epId");


    await db.collection("data").findOneAndUpdate(
      { id: showId, "seasons.n": seasonTitle },
      {
        //@ts-ignore
        $pull: { "seasons.$.eps": { ep: eptitle } },
      }
    );


    return new Response(
      JSON.stringify({
        message: "Received request: " + intent,
      }),
      { status: 200 }
    );
  }
  else {
    return new Response(
      JSON.stringify({
        message: "uai, tem nd aqui nn."
      }),
      { status: 404 }
    );
  }
  // Validate the data - you'll probably want to do more than this
  /*if (!name || !email || !message) {
    return new Response(
      JSON.stringify({
        message: "Missing required fields",
      }),
      { status: 400 }
    );
  }*/
  // Do something with the data, then return a success response
};