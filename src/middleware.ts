import { defineMiddleware } from "astro:middleware";

export const onRequest = defineMiddleware((context, next) => {
  // If a basic auth header is present, it wil take the string form: "Basic authValue"
  if (context.url.pathname === "/dashboard") {
    const basicAuth = context.request.headers.get("authorization");
    if (basicAuth) {
      const authValue = basicAuth.split(" ")[1] ?? "username:password";
      // Decode the Base64 encoded string via atob (https://developer.mozilla.org/en-US/docs/Web/API/atob)
      const [username, pwd] = atob(authValue).split(":");
      if (username === process.env.ADMNAME && pwd === process.env.ADMPASS) {
        return next();
      }
    }

return new Response("Auth required", {
  status: 401,
  headers: {
    "WWW-authenticate": 'Basic realm="Secure Area"',
  },
});
  } else {
  return next();
}
});
